import React, { useContext } from 'react';
import { Route, Navigate  } from 'react-router-dom';
import { AuthProvider } from '../Components/Context/AuthContext';

const AuthenticatedRoute = ({ component: Component, ...rest }) => {
  const { isLoggedIn } = useContext(AuthProvider);

  return (
    <Route
      {...rest}
      render={props =>
        isLoggedIn ? <Component {...props} /> : <Navigate  to="/" />
      }
    />
  );
};

export default AuthenticatedRoute;