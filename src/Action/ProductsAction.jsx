import { SET_INITIAL_PRODUCTS,GET_INITIAL_PRODUCTS,SEARCH_PRODUCTS } from "../Utils/Constant";
export const setInitialProducts = (array) => ({
  type: SET_INITIAL_PRODUCTS,
  payload: array,
});

export const searchProducts = (string) => ({
    type:SEARCH_PRODUCTS,
    payload:string
})
export const getInitialProducts = (string) => ({
    type:GET_INITIAL_PRODUCTS,
    payload:string
})
