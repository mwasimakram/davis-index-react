import {Navbar,Button,Container} from 'react-bootstrap';
import { useAuth } from '../Context/AuthContext';
function TopHeader() {
    const {logout} = useAuth();
    return (
        <>
            <Navbar bg='light'>
                <Container>
                    <Navbar.Brand href="#home">Davis Index</Navbar.Brand>
                    <Navbar.Toggle />
                    <Navbar.Collapse className="justify-content-end">
                        <Navbar.Text>
                            <Button variant="link" onClick={logout}>LogOut</Button>
                        </Navbar.Text>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
        </>
    );
}

export default TopHeader;