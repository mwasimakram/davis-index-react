import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import './RightSideMenu.css';
import {SizeOpen,SizeClose} from '../../Utils/Helper';
import editing from "../../images/editing.png"
import menu from "../../images/left.png"
import email from "../../images/email.png"
const RightSideMenu = () => {
  return (
    <div className="container-fluid">
      <div className="row">
        <div className="col-md-3 right" id="rightt">
          <div className="bg-light row">
            {/* <h2>Sidebar Content</h2> */}
            {/* Sidebar content goes here */}
            <div className="row">
            <div className="col-sm-3 icons" id="icon">
                <div className="border-bottomm" id="bottom">
                <img src={editing} width={40} id="edit" className="menu-img-bg-light"/>
                <img src={menu} width={40} onClick={SizeOpen} id="mobile-menu" className="toggle"/>
                <img src={require('../../images/right1.png')} width={40} onClick={SizeClose} id="close" className="toggle"/>
                </div>
                <div className="border-bottomm" id="bottom">
                <img src={email} width={40} id="email" className="menu-img"/>
                <img src={require('../../images/link.png')} width={40} id="sedan" className="menu-img"/>
                </div>
                <div className="border-bottomm" id="bottom">
                <img src={require('../../images/blank-page.png')} width={40} id="blank" className="menu-img"/>
                <img src={require('../../images/user.png')} width={40} id="user" className="menu-img"/>
                </div>
                <img src={require('../../images/information.png')} width={40} id="information" className="menu-img"/>
            </div>
            <div className="col-sm-9 dynamic" id="dyna">
                <div className="outer outer1">
                    <h2 className="outer-heading">HUB</h2>
                    <div className="inner inner1">
                        <div className="data-heading">
                        <h4 className="inner-heading">STATUS</h4>
                        </div>
                        <div className="data">
                        <ul>
                            <li>In-progress</li>
                            <li>Completed</li>
                            <li>Withdrawn</li>
                            <li>Declined</li>
                            <li>Cancelled</li>
                        </ul>
                        </div>
                    </div>
                    <div className="inner inner2">
                        <div className="data-heading">
                        <h4 className="inner-heading">TIME</h4>
                        </div>
                        <div className="data">
                        <ul>
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                20
                                </button>
                            </div>
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                March
                                </button>
                            </div>
                            <div className="dropdown">
                                <button className="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                2023
                                </button>
                            </div>
                            <li>Today</li>
                            <li>Yesterday</li>
                            <li>Last Seven days</li>
                            <li>This Month</li>
                            <li>Last Month</li>
                            <li>This Year</li>
                            <li>Last Year</li>
                        </ul>
                        </div>
                    </div>
                </div>
                <div className="outer outer2">
                    <h2 className="outer-heading">PRICE</h2>
                    <div className="inner inner1">
                        <h4 className="inner-heading">MATERIAL TYPE</h4>
                        <div className="data">
                        <ul className="last-ul">
                            <li>Ferro-alloys</li>
                            <li>Ferrous</li>
                            <li>Electronics</li>
                            <li>Mixed metals</li>
                            <li>Minor metals</li>
                            <li>Precious metals</li>
                            <li>Speciality Alloys</li>
                        </ul>
                        </div>
                        <button className="reset">Reset</button>
                        <button className="apply">Apply</button>
                    </div>
                </div>                
            </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );  
};

export default RightSideMenu;