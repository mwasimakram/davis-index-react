import { Form, Button, Container, Row, Col } from 'react-bootstrap';
import { useState, useContext } from 'react';
import { useAuth } from '../Context/AuthContext';
export default function SignIn() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { login } = useAuth();
    const handleLogin = () => {
        login({ email: email, password: password });
    };
    return (
        <Container>
            <Row>
                <Col xs={3}></Col>
                <Col xs={6}>
                    <h3 className='text-align-center mt-3'>Please Login here!</h3>
                    <Form inline>
                        <Form.Control type="email" className='mt-3' placeholder="Email" onChange={(e) => setEmail(e.target.value)} />
                        <Form.Control type="password" className='mt-3' placeholder="Password" onChange={(e) => setPassword(e.target.value)} />
                        <Button variant="outline-success" className='mt-3' onClick={handleLogin}>Login</Button>
                    </Form>
                </Col>
                <Col xs={3}></Col>

            </Row>
        </Container>

    )
}