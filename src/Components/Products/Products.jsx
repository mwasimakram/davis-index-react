import { PRODUCT_LIST } from "../../Utils/Constant";
import { getReq } from "../../Services/axios";
import { useState, useEffect } from "react";
import Search from "../Common/Search";
import { useDispatch ,useSelector} from "react-redux";
import { setInitialProducts , searchProducts ,getInitialProducts} from "../../Action/ProductsAction";
import "./Product.css";
export default function Product() {
    const dispatch = useDispatch();
    console.log(useSelector(state => state))
    const products = useSelector(state => state.product.searchResult)
    useEffect(() => {
        const getProductList = async () => {
            const response = await getReq(PRODUCT_LIST);
            console.log(response.data)
            dispatch(setInitialProducts(response.data));
        }
        getProductList();
    }, []);
    function getSearchString(query) {
        if(!query){
            dispatch(getInitialProducts(""))
            console.log(dispatch(getInitialProducts("")))
        }else{
            dispatch(searchProducts(query))
        }
    }
    return (<>
        <Search onChangeInput={getSearchString}></Search>
        {products && products.length > 0 ? (
            products.map((p) => {
                return <div className="main-data-list" id="data-list">
                    <div className="container">
                        <div className="row data-row">
                            <div className="col-md-6">
                                <table>
                                    <tbody>
                                        <tr>
                                            <td className="title"><h3>Cement</h3></td>
                                            <td><button className="export">Export</button></td>
                                            <td><img src={require('../../images/star.png')} width={15}/><p className="rating">4/5<span>Rating</span></p></td>
                                        </tr> 
                                        <tr>
                                            <td><p><b>600AED/mt</b></p></td>
                                            <td><input type="checkbox" name="status"/> status</td>
                                            <td><p>1 Bookmarks</p></td>
                                        </tr>
                                        <tr>
                                            <div className="row inner location">
                                                <div className="col-md-12 col-loc">
                                                <img src={require('../../images/pin.png')} width={15} className="pin"/> <p className="location"><span> lorem, Afghanistan</span></p>
                                                </div>
                                            </div>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            
                            <div className="col-md-5">
                            <table>
                                    <tbody>
                                        <tr>
                                            <td><p><b>10 USD/KG</b></p></td>
                                            <td><b>0/0</b></td>
                                        </tr>
                                        <tr>
                                            <td><p>Transactions</p></td>
                                            <td><p>Offers</p></td>
                                        </tr><br/>
                                        <tr>
                                            <td><b>0/0</b></td>
                                            <td><b>0</b></td>
                                        </tr>
                                        <tr>
                                            <td><p>Pending Reviews</p></td>
                                            <td><p>Attachments</p></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div className="col-md-1">
                                <button className="sub">Submit Offer</button>
                            </div>

                            {/* <h3>{p.title}</h3>
                            <p>{p.description}</p>
                            <p>{p.price}</p>
                            <p>{p.discountPercentage}</p>
                            <p>{p.rating}</p>
                            <p>{p.stock}</p>
                            <p>{p.brand}</p>
                            <p>{p.category}</p> */}
                        </div>
                    </div>
                </div>
            })
        ) : (
            <p>Loading...</p>
        )}
    </>)
}