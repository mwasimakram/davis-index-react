import { createContext, useContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';

const AuthContext = createContext();

function AuthProvider({ children }) {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  const navigate = useNavigate();

  const login = (userCrediential = {}) => {
    console.log(userCrediential);
    const { email, password } = userCrediential;
    if (email === 'testuser@gmail.com' && password === 'user@123') {
      setIsLoggedIn(true);
      navigate('/home')
    }
  };

  const logout = () => {
    console.log("logout called")
    setIsLoggedIn(false);
    navigate('/');
  };

  return (
    <AuthContext.Provider value={{ isLoggedIn, login, logout }}>
      {children}
    </AuthContext.Provider>
  );
}

function useAuth() {
  return useContext(AuthContext);
}

export { AuthProvider, useAuth };