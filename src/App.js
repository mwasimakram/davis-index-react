import logo from './logo.svg';
import './App.css';
import { AuthProvider } from './Components/Context/AuthContext';
import RoutesList from './Routes/Routes';
import { BrowserRouter } from 'react-router-dom';

function App() {
  return (
    <BrowserRouter>
      <AuthProvider>
        <RoutesList />
      </AuthProvider>
    </BrowserRouter>
  );
}

export default App;
