import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "../Redux/Index";
 const store = configureStore({ reducer: rootReducer });
 export default store;

